<?php

namespace App\Interfaces;

use App\Models\User;

interface AuthInterface
{
    /**
     * Create new user with given credentials
     *
     * @param array $data
     * @return User
     */
    public function register(array $data): User;

    /**
     * Attempt login user with given credentials and return token if success
     *
     * @param array $data
     * @return string
     */
    public function login(array $data): string;
}
