<?php

namespace App\Repositories;

use App\Interfaces\GlossaryRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class GlossaryRepository implements GlossaryRepositoryInterface
{
    /**
     * @param Model $model
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function get(Model $model, int $perPage = null): LengthAwarePaginator
    {
        return $model::query()->paginate($perPage);
    }

    /**
     * @param Model $model
     * @param int $id
     * @return Model|null
     */
    public function findById(Model $model, int $id): Model|null
    {
        return $model::query()->findOrFail($id);
    }

    /**
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function create(Model $model, array $data): Model
    {
        return $model::query()->create($data);
    }

    /**
     * @param Model $model
     * @param int $id
     * @return Model
     */
    public function show(Model $model, int $id): Model
    {
        return $this->findById($model, $id);
    }

    /**
     * @param Model $model
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(Model $model, int $id, array $data): Model
    {
        $instance = $this->findById($model, $id);

        if ($instance) {
            $instance->fill($data);
            $instance->save();
        }

        return $instance;
    }

    /**
     * @param Model $model
     * @param int $id
     * @return void
     */
    public function delete(Model $model, int $id): void
    {
        $model = $this->findById($model, $id);

        $model->delete();
    }
}
