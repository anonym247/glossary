<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface GlossaryRepositoryInterface
{
    /**
     * @param Model $model
     * @param int $id
     * @return Model|null
     */
    public function findById(Model $model, int $id): Model|null;

    /**
     * @param Model $model
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function get(Model $model, int $perPage = null): LengthAwarePaginator;

    /**
     * @param Model $model
     * @param array $data
     * @return Model
     */
    public function create(Model $model, array $data): Model;

    /**
     * @param Model $model
     * @param int $id
     * @return Model
     */
    public function show(Model $model, int $id): Model;

    /**
     * @param Model $model
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(Model $model, int $id, array $data): Model;

    /**
     * @param Model $model
     * @param int $id
     * @return void
     */
    public function delete(Model $model, int $id): void;
}
