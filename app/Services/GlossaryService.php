<?php

namespace App\Services;

use App\Interfaces\GlossaryRepositoryInterface;
use App\Interfaces\GlossaryServiceInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class GlossaryService implements GlossaryServiceInterface
{
    /**
     * @param GlossaryRepositoryInterface $glossaryRepository
     */
    public function __construct(private GlossaryRepositoryInterface $glossaryRepository) {}

    /**
     * @param string $modelClass
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function index(string $modelClass, int $perPage = null): LengthAwarePaginator
    {
        return $this->glossaryRepository->get(getModelInstance($modelClass), $perPage);
    }

    /**
     * @param string $modelClass
     * @param array $data
     * @return Model
     */
    public function store(string $modelClass, array $data): Model
    {
        return $this->glossaryRepository->create(getModelInstance($modelClass), $data);
    }

    /**
     * @param string $modelClass
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(string $modelClass, int $id, array $data): Model
    {
        return $this->glossaryRepository->update(getModelInstance($modelClass), $id, $data);
    }

    /**
     * @param string $modelClass
     * @param int $id
     * @return Model
     */
    public function show(string $modelClass, int $id): Model
    {
        return $this->glossaryRepository->findById(getModelInstance($modelClass), $id);
    }

    /**
     * @param string $modelClass
     * @param int $id
     * @return void
     */
    public function delete(string $modelClass, int $id): void
    {
        $this->glossaryRepository->delete(getModelInstance($modelClass), $id);
    }
}
