<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GlossaryModelTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    /**
     * @test Can glossary service create art-type
     */
    public function canAuthorizedUserCreateArtType()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $data = [
            'title' => 'Test type'
        ];

        $response = $this->post(route('glossary.store', ['model' => 'art-types']), $data);
        $response->assertOk();
    }

    /**
     * @test Can glossary service create art-type
     */
    public function canUnauthorizedUserCreateArtType()
    {
        $data = [
            'title' => 'Test type'
        ];

        $response = $this->post(route('glossary.store', ['model' => 'art-types']), $data, [
            'Accept' => 'application/json'
        ]);
        $response->assertUnauthorized();
    }

    public function testUnprocessableEntityWhileCreateArtType()
    {
        $user = User::factory()->create();
        $this->actingAs($user);

        $data = [];

        $response = $this->post(route('glossary.store', ['model' => 'art-types']), $data, [
            'Accept' => 'application/json'
        ]);
        $response->assertUnprocessable();
        $response->assertJsonValidationErrorFor('title');
    }
}
