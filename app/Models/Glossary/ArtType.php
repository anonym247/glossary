<?php

namespace App\Models\Glossary;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtType extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['title'];

    protected $hidden = ['updated_at', 'deleted_at'];
}
