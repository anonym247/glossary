<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface GlossaryServiceInterface
{
    /**
     * @param string $modelClass
     * @param int|null $perPage
     * @return LengthAwarePaginator
     */
    public function index(string $modelClass, int $perPage = null): LengthAwarePaginator;

    /**
     * @param string $modelClass
     * @param array $data
     * @return Model
     */
    public function store(string $modelClass, array $data): Model;

    /**
     * @param string $modelClass
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(string $modelClass, int $id, array $data): Model;

    /**
     * @param string $modelClass
     * @param int $id
     * @return Model
     */
    public function show(string $modelClass, int $id): Model;

    /**
     * @param string $modelClass
     * @param int $id
     * @return void
     */
    public function delete(string $modelClass, int $id): void;
}
