<?php

namespace App\Http\Requests\Glossary;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        $model = getModelInstance($this->segment(3));

        $rules = [];

        foreach ($model->getFillable() as $item) {
            $rules[$item] = 'nullable';
        }

        return $rules;
    }
}
