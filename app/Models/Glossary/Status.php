<?php

namespace App\Models\Glossary;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'color'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
