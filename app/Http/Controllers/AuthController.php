<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Interfaces\AuthInterface;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class AuthController extends Controller
{
    /**
     * @param AuthInterface $authService
     */
    public function __construct(public AuthInterface $authService) {}

    /**
     * @OA\RequestBody(
     *     request="RegisterRequest",
     *     required=true,
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @OA\Post(
     *     path="/api/auth/register",
     *     tags={"auth"},
     *     operationId="register",
     *     @OA\Response(
     *         response=200,
     *         description="Success"
     *     ),
     *     @OA\RequestBody(ref="#/components/requestBodies/RegisterRequest")
     * )
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        $this->authService->register($request->validated());

        return response()->json('', 204);
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        return response()->json(['token' => $this->authService->login($request->validated())]);
    }
}
