<?php

namespace App\Http\Controllers;

use App\Http\Requests\Glossary\CreateRequest;
use App\Http\Requests\Glossary\UpdateRequest;
use App\Interfaces\GlossaryServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GlossaryController extends Controller
{
    /**
     * @param GlossaryServiceInterface $glossaryService
     */
    public function __construct(private GlossaryServiceInterface $glossaryService) {}

    /**
     * @param Request $request
     * @param string $model
     * @return JsonResponse
     */
    public function index(Request $request, string $model): JsonResponse
    {
        return response()->json([
            'data' => $this->glossaryService->index($model, $request->get('per_page'))
        ]);
    }

    /**
     * @param CreateRequest $request
     * @param string $model
     * @return JsonResponse
     */
    public function store(CreateRequest $request, string $model): JsonResponse
    {
        return response()->json([
            'data' => $this->glossaryService->store($model, $request->validated())
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @param string $model
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, string $model, int $id): JsonResponse
    {
        return response()->json([
            'data' => $this->glossaryService->update($model, $id, $request->validated())
        ]);
    }

    /**
     * @param string $model
     * @param int $id
     * @return JsonResponse
     */
    public function show(string $model, int $id): JsonResponse
    {
        return response()->json([
            'data' => $this->glossaryService->show($model, $id)
        ]);
    }

    /**
     * @param string $model
     * @param int $id
     * @return JsonResponse
     */
    public function delete(string $model, int $id): JsonResponse
    {
        $this->glossaryService->delete($model, $id);

        return response()->json('', 204);
    }
}
