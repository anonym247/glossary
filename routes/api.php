<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\GlossaryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('register', [AuthController::class, 'register'])
        ->name('auth.register');
    Route::post('login', [AuthController::class, 'login'])
        ->name('auth.login');
});

Route::prefix('glossary')->middleware(['auth:sanctum'])->group(function () {
    Route::resource('{model}', GlossaryController::class)
        ->except(['update', 'show', 'edit', 'destroy', 'create'])
        ->names(['store' => 'glossary.store', 'index' => 'glossary.index']);
    Route::put('{model}/{id}', [GlossaryController::class, 'update'])
        ->name('glossary.update');
    Route::get('{model}/{id}', [GlossaryController::class, 'show'])
        ->name('glossary.show');
    Route::delete('{model}/{id}', [GlossaryController::class, 'delete'])
        ->name('glossary.delete');
});
