<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

if (!function_exists('makeModelPath')) {
    function makeModelPath(string $tableName): string {
        $finalName = '';

        $parts = explode('-', $tableName);

        $pluralPart = end($parts);

        for ($i = 0; $i < count($parts) - 1; $i++) {
            $finalName .= Str::ucfirst($parts[$i]);
        }

        $finalName .= Str::ucfirst(Str::singular($pluralPart));

        return 'App\Models\Glossary\\' . $finalName;
    }
}

if (!function_exists('getModelInstance')) {
    function getModelInstance(string $resourceName): Model {
        try {
            return app(makeModelPath($resourceName));
        } catch (\Exception $exception) {
            throw new ModelNotFoundException($resourceName);
        }
    }
}
