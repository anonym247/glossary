<?php

namespace App\Providers;

use App\Interfaces\AuthInterface;
use App\Interfaces\GlossaryRepositoryInterface;
use App\Interfaces\GlossaryServiceInterface;
use App\Repositories\GlossaryRepository;
use App\Services\AuthService;
use App\Services\GlossaryService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AuthInterface::class, AuthService::class);
        $this->app->bind(GlossaryRepositoryInterface::class, GlossaryRepository::class);
        $this->app->bind(GlossaryServiceInterface::class, function () {
            return new GlossaryService(app(GlossaryRepositoryInterface::class));
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
