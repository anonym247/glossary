<?php

namespace App\Services;

use App\Interfaces\AuthInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class AuthService implements AuthInterface
{
    /**
     * @inheritDoc
     */
    public function register(array $data): User
    {
        $user = new User();

        $user->fill($data);
        $user->password = bcrypt($data['password']);

        $user->save();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function login(array $data): string
    {
        /**
         * @var User $user
         */
        $user = User::query()->where('email', $data['email'])->firstOrFail();

        if (!Auth::attempt($data)) {
            throw new ModelNotFoundException('Wrong Credentials', 422);
        }

        return $user->createToken('Bearer')->plainTextToken;
    }
}
